package com.nexus8international.android.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.nexus8international.android.R;
import com.nexus8international.android.fragments.ListDialogFragment;
import com.nexus8international.android.fragments.RecyclerViewFragment;
import com.nexus8international.android.fragments.SearchFragment;

public class BrowseCasesActivity extends AppCompatActivity implements
		SearchFragment.OnSearchListener, ListDialogFragment.OnDialogItemSelectedListener {

	private static final int PRIORITY_FILTER_MODE = 1;

	private static final int CATEGORY_FILTER_MODE = 2;

	private ListDialogFragment mDialogFragment;

	private RecyclerViewFragment mRecyclerViewFragment;
	private String mFilterValue1;

	private String mFilterValue2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browse_cases);
		setSupportActionBar(findViewById(R.id.browse_cases_activity_toolbar));
		if (savedInstanceState == null) {
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			SearchFragment searchFragment = new SearchFragment();
			searchFragment.setArguments(getIntent().getExtras());
			mRecyclerViewFragment = new RecyclerViewFragment();
			mRecyclerViewFragment.setArguments(getIntent().getExtras());
			transaction.replace(R.id.search_fragment, searchFragment);
			transaction.replace(R.id.cases_fragment, mRecyclerViewFragment);
			transaction.commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_browse_cases, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_filter:
				mDialogFragment = ListDialogFragment.newInstance(ListDialogFragment.FILTER_SELECTION_MODE, this);
				mDialogFragment.show(getSupportFragmentManager(), getResources().getString(R.string.action_filter_result));
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSearchByTopic(String searchTerm) {
		mRecyclerViewFragment.updateResult(searchTerm);
	}

	public void setToolbarTitle(int resourceId) {
		if (getSupportActionBar() != null) getSupportActionBar().setTitle(resourceId);
	}

	@Override
	public void onSelected(int mode, int pos) {
		switch (mode) {
			case ListDialogFragment.FILTER_SELECTION_MODE:
				if (pos == 0) {
					mDialogFragment = ListDialogFragment.newInstance(ListDialogFragment.RESPONSE_LEVEL_MODE, this);
					mDialogFragment.show(getSupportFragmentManager(), getResources().getString(R.string.filter_by_priority));
				} else {
					mDialogFragment = ListDialogFragment.newInstance(ListDialogFragment.CATEGORY_MODE, this);
					mDialogFragment.show(getSupportFragmentManager(), getResources().getString(R.string.filter_by_category));
				}
				return;
			case ListDialogFragment.CATEGORY_MODE:
				mFilterValue1 = String.valueOf(getResources().getIntArray(R.array.category_ids)[pos]);
				mDialogFragment = ListDialogFragment.newInstance(ListDialogFragment.SUB_CATEGORY_MODES[pos], this);
				mDialogFragment.show(getSupportFragmentManager(), getResources().getString(R.string.filter_by_category));
				return;
			case ListDialogFragment.RESPONSE_LEVEL_MODE:
				mFilterValue1 = getResources().getStringArray(R.array.response_levels)[pos];
				mRecyclerViewFragment.updateFilter(PRIORITY_FILTER_MODE, mFilterValue1, null);
				break;
			case ListDialogFragment.SUB_CATEGORY0_MODE:
				mFilterValue2 = String.valueOf(getResources().getIntArray(R.array.sub_category_0_ids)[pos]);
				mRecyclerViewFragment.updateFilter(CATEGORY_FILTER_MODE, mFilterValue1, mFilterValue2);
				break;
			case ListDialogFragment.SUB_CATEGORY1_MODE:
				mFilterValue2 = String.valueOf(getResources().getIntArray(R.array.sub_category_1_ids)[pos]);
				mRecyclerViewFragment.updateFilter(CATEGORY_FILTER_MODE, mFilterValue1, mFilterValue2);
				break;
			case ListDialogFragment.SUB_CATEGORY2_MODE:
				mFilterValue2 = String.valueOf(getResources().getIntArray(R.array.sub_category_2_ids)[pos]);
				mRecyclerViewFragment.updateFilter(CATEGORY_FILTER_MODE, mFilterValue1, mFilterValue2);
				break;
		}
		mRecyclerViewFragment.sendNetworkRequest();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.enter_2, R.anim.exit_2);
	}
}
