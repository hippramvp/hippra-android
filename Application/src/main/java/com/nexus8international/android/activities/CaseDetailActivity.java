package com.nexus8international.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.nexus8international.android.R;
import com.nexus8international.android.common.utils.HippraApi;
import com.nexus8international.android.fragments.CaseDetailFragment;
import com.nexus8international.android.fragments.ListResponseFragment;
import com.nexus8international.android.fragments.ReplyFragment;

public class CaseDetailActivity extends AppCompatActivity implements ReplyFragment.OnReplySentListener,
        ListResponseFragment.OnListResponseListener {
    private CaseDetailFragment mCaseDetailFragment;
    private ReplyFragment mReplyFragment;
    private ListResponseFragment mListResponseFragment;
    private int mCaseID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case_detail);
        setSupportActionBar(findViewById(R.id.detail_case_activity_toolbar));
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Bundle bundle = getIntent().getExtras();
            mCaseID = (bundle == null) ? -1 : bundle.getInt(HippraApi.CASE_ID_FLAG, -1);

            mCaseDetailFragment = CaseDetailFragment.newInstance(mCaseID);
            mListResponseFragment = ListResponseFragment.newInstance(this, mCaseID);
            mReplyFragment = ReplyFragment.newInstance(this, mCaseID);
            transaction.replace(R.id.case_detail_fragment, mCaseDetailFragment);
            transaction.replace(R.id.list_response_fragment, mListResponseFragment);
            transaction.replace(R.id.reply_fragment, mReplyFragment);
            transaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_case_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share_via_messenger:
                //share via messenger
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "*HIPPRA* We Need Your Input");
                String str = "[" +
                        mCaseDetailFragment.getTopic() +
                        ']' +
                        "\nPlease download the HIPPRA app to response to this case" +
                        "\niOS app: https://itunes.apple.com/us/app/hippra/id1173474322?mt=8\n" +
                        "\nAndroid app: https://play.google.com/store/apps/details?id=com.nexus8international.android\n" +
                        "\nAbout HIPPRA\n" +
                        getResources().getStringArray(R.array.static_text_content)[0];
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, str);
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //load the response list
        onUpdateResponseList();
    }

    @Override
    public void onReply() {
        mListResponseFragment.updateResponseList();
    }

    @Override
    public void onUpdateResponseList() {
        mListResponseFragment.updateResponseList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_2, R.anim.exit_2);
    }
}
