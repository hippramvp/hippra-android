package com.nexus8international.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.nexus8international.android.R;
import com.nexus8international.android.common.json_class.HippraBaseResponse;
import com.nexus8international.android.common.json_class.User;
import com.nexus8international.android.common.utils.HippraApi;
import com.nexus8international.android.common.utils.Prefs;
import com.nexus8international.android.fragments.ListDialogFragment;
import com.nexus8international.android.fragments.home_fragments.ChangePasswordFragment;
import com.nexus8international.android.fragments.home_fragments.HomeScreenFragment;
import com.nexus8international.android.fragments.home_fragments.HomeScreenFragment.OnFragmentInteractionListener;
import com.nexus8international.android.fragments.home_fragments.UserProfileFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity implements
		ListDialogFragment.OnDialogItemSelectedListener, OnFragmentInteractionListener {


	AlertDialog mLogoutDialog;

	AlertDialog mInfoDialog;

	private ListDialogFragment mDialogFragment;

	private HomeScreenFragment mHomeScreenFragment;

	private ChangePasswordFragment mChangePasswordFragment;
	private UserProfileFragment mUserProfileFragment;
	private FirebaseAnalytics mFirebaseAnalytics;

	@BindView(R.id.home_screen_layout)
	ViewGroup mHomeScreenLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		ButterKnife.bind(this);
		Prefs.getInstance(this);
		//TODO read https://firebase.google.com/docs/reference/android/com/google/firebase/analytics/FirebaseAnalytics.Event.html#APP_OPEN
		mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
		Bundle logBundle = new Bundle();
		logBundle.putString(FirebaseAnalytics.Param.ITEM_ID, "0");
		logBundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "test");
		logBundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "null");
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, logBundle);
		logBundle = new Bundle();
		logBundle.putString(FirebaseAnalytics.Param.ITEM_ID, "0");
		logBundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "test2");
		logBundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, logBundle);
		if (savedInstanceState == null) {
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			mChangePasswordFragment = new ChangePasswordFragment();
			mUserProfileFragment = new UserProfileFragment();
			mHomeScreenFragment = new HomeScreenFragment();
			transaction.replace(R.id.home_screen_layout, mHomeScreenFragment);
			transaction.commit();
		}

		mDialogFragment = ListDialogFragment.newInstance(ListDialogFragment.SETTINGS_SELECTION_MODE, this);

		setSupportActionBar(findViewById(R.id.home_activity_toolbar));
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("test message 1").setTitle("title 1").setCancelable(true)
				.setPositiveButton(R.string.ok, null);
		mInfoDialog = builder.create();
	}

	@Override
	protected void onResume() {
		super.onResume();
		//always show the home screen first when launching this activity
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.home_screen_layout, mHomeScreenFragment);
		transaction.commit();
		if (!Prefs.isLoggedIn()) {
			startActivity(new Intent(this, LoginActivity.class));
			overridePendingTransition(R.anim.enter, R.anim.exit);
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.confirm_sign_out).setTitle(R.string.action_sign_out).setCancelable(false);
		builder.setNegativeButton(R.string.no_str, null);
		builder.setPositiveButton(R.string.yes_str, (dialog, which) -> {
			//send the http request to sign out
			User user = new User(Prefs.getUserName(), Prefs.getDeviceType());
			//create retrofit instance
			Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
					.baseUrl("https://api.hippra.com/api/v1/")
					.addConverterFactory(GsonConverterFactory.create());
			Retrofit retrofit = retrofitBuilder.build();
			HippraApi hippra = retrofit.create(HippraApi.class);
			Call<HippraBaseResponse> call = hippra.signOut(user);

			call.enqueue(new Callback<HippraBaseResponse>() {
				@Override
				public void onResponse(Call<HippraBaseResponse> call, Response<HippraBaseResponse> response) {
					//TODO do something after the backend update
					//Log.d("logout", "logout response is " + response.isSuccessful());
				}

				@Override
				public void onFailure(Call<HippraBaseResponse> call, Throwable t) {
					//TODO do something after the backend update
				}

			});
			// clear some fields in the singleton Prefs instance
			Prefs.clear();
			//Log.d("home activity", "is logged in " + Prefs.isLoggedIn());
			startActivity(new Intent(HomeActivity.this, LoginActivity.class));
			overridePendingTransition(R.anim.enter, R.anim.exit);
		});
		mLogoutDialog = builder.create();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_help:
				mDialogFragment.show(getSupportFragmentManager(), "Help");
				break;
			case R.id.action_sign_out:
				mLogoutDialog.show();
				mLogoutDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.PinkFontColor));
				mLogoutDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.PinkFontColor));

				return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onSelected(int mode, int pos) {
		mInfoDialog.setTitle(getResources().getStringArray(R.array.setting_options)[pos]);
		if (pos != 4) {
			mInfoDialog.setMessage(getResources().getStringArray(R.array.static_text_content)[pos]);
			mInfoDialog.show();
			mInfoDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.PinkFontColor));
		} else {
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.home_screen_layout, mChangePasswordFragment);
			//push the transaction to the fragment back stack
			transaction.addToBackStack(null);
			transaction.commit();
		}
	}

	@Override
	public void onProfileBtnPressed() {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.home_screen_layout, mUserProfileFragment);
		//push the transaction to the fragment back stack
		transaction.addToBackStack(null);
		transaction.commit();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.enter_2, R.anim.exit_2);
	}
}
