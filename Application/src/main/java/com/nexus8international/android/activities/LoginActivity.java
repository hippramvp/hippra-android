package com.nexus8international.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.nexus8international.android.R;
import com.nexus8international.android.fragments.sigin_signup_fragments.LoginFragment;
import com.nexus8international.android.fragments.sigin_signup_fragments.WelcomeFragment;

public class LoginActivity extends AppCompatActivity implements WelcomeFragment.OnFragmentInteractionListener,
        LoginFragment.OnFragmentInteractionListener {
    WelcomeFragment mWelcomeFragment;
    LoginFragment mLoginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mWelcomeFragment = new WelcomeFragment();
        mLoginFragment = new LoginFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.login_fragment, mWelcomeFragment);
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(int actionID) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (actionID) {
            case R.id.sign_in_btn:
                transaction.replace(R.id.login_fragment, mLoginFragment);
                break;
            case R.id.register_btn:
                startActivity(new Intent(this, RegistrationActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.submit_sign_in_btn:
                startActivity(new Intent(this, HomeActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
        }
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_2, R.anim.exit_2);
    }
}
