package com.nexus8international.android.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nexus8international.android.R;
import com.nexus8international.android.fragments.NewCaseFragment;

public class NewCaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_case);
        setSupportActionBar(findViewById(R.id.new_case_activity_toolbar));
        if (savedInstanceState == null) {
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            //TODO get bundle and implement newInstance() for this fragment
            transaction.replace(R.id.new_case_fragment, new NewCaseFragment());
            transaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_2, R.anim.exit_2);
    }
}
