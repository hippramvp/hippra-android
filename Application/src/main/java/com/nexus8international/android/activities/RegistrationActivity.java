package com.nexus8international.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.nexus8international.android.R;
import com.nexus8international.android.common.json_class.UserProfile;
import com.nexus8international.android.fragments.RegisterAccountFragment;

public class RegistrationActivity extends AppCompatActivity implements RegisterAccountFragment.OnRegisteredListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.register_frame_layout, new RegisterAccountFragment());
        transaction.commit();
    }

    @Override
    public void onRegistered(UserProfile profile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.wait_for_email_str).setTitle("Thank You").setCancelable(false);
        builder.setPositiveButton(R.string.ok, (dialog, id) -> {
            startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.PinkFontColor));
    }
}
