package com.nexus8international.android.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nexus8international.android.R;
import com.nexus8international.android.activities.CaseDetailActivity;
import com.nexus8international.android.adapters.viewholders.DoubleLayerViewHolder;
import com.nexus8international.android.common.json_class.Case;
import com.nexus8international.android.common.utils.HippraApi;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CaseAdapter extends RecyclerView.Adapter<CaseAdapter.ViewHolder> {

    private static final String TAG = "CaseAdapter";

    public ArrayList<Case> getDataList() {
        return mDataList;
    }

    private ArrayList<Case> mDataList;

    public CaseAdapter() {
        mDataList = new ArrayList<>();
    }

    public CaseAdapter(ArrayList<Case> data) {
        mDataList = new ArrayList<>(data);
    }

    public boolean updateData(ArrayList<Case> data) {
        mDataList.clear();
        mDataList.addAll(data);
        notifyDataSetChanged();
        return data != null;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.case_item_card, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        Case current = mDataList.get(position);
        viewHolder.topicTextView.setText(current.getTitle());
        String full = current.getMedicalSpecialityName() + "/" + current.getSubMedicalSpecialityName();
        viewHolder.catTextView.setText(full);
        viewHolder.attenTextView.setText(current.getResponse_needed());
        viewHolder.descTextView.setText(current.getDescription());
        viewHolder.timeTextView.setText(current.getCreated_at());
    }

    @Override
    public int getItemCount() {
        return (mDataList == null) ? 0 : mDataList.size();
    }

    public void removeItem(int deletedIndex) {
        mDataList.remove(deletedIndex);
        //not using notify data set changed, notify by index
        notifyItemRemoved(deletedIndex);
    }

    public void restoreItem(Case deletedItem, int deletedIndex) {
        mDataList.add(deletedIndex, deletedItem);
        //not using notify data set changed, notify item added by position
        notifyItemInserted(deletedIndex);
    }

    public class ViewHolder extends DoubleLayerViewHolder {
        @BindView(R.id.topic_text_view)
        TextView topicTextView;
        @BindView(R.id.category_text_view)
        TextView catTextView;
        @BindView(R.id.response_needed_text_view)
        TextView attenTextView;
        @BindView(R.id.desc_text_view)
        TextView descTextView;
        @BindView(R.id.date_time_text_view)
        TextView timeTextView;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            foreground = v.findViewById(R.id.foreground_card);
            v.setOnClickListener(v1 -> {
                Context context = v1.getContext();
                Intent intent = new Intent(context, CaseDetailActivity.class);
                int id = CaseAdapter.this.mDataList.get(getAdapterPosition()).getId();
                intent.putExtra(HippraApi.CASE_ID_FLAG, id);
                //Log.d(TAG, "Element " + getAdapterPosition() + " clicked.");
                //Log.d(TAG, "The requested case is: " + Integer.toString(id));
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.enter, R.anim.exit);
            });
        }
    }
}
