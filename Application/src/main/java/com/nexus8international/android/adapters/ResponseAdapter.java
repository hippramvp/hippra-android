package com.nexus8international.android.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nexus8international.android.R;
import com.nexus8international.android.adapters.viewholders.DoubleLayerViewHolder;
import com.nexus8international.android.common.json_class.Reply;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResponseAdapter extends RecyclerView.Adapter<ResponseAdapter.ResponseViewHolder> {

    private ArrayList<Reply> mResponses;

    public ResponseAdapter() {
        mResponses = new ArrayList<>();
    }

    public ArrayList<Reply> getResponses() {
        return mResponses;
    }

    @NonNull
    @Override
    public ResponseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.response_item_card, parent, false);
        return new ResponseViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ResponseViewHolder holder, int position) {
        Reply current = mResponses.get(position);
        String name = current.getFirst_name() + " " + current.getLast_name();
        holder.mAuthor.setText(name);
        holder.mAuthorSpecialty.setText(current.getMedical_speciality());
        holder.mResponse.setText(current.getValue());
        holder.mTime.setText(current.getCreated_at());
    }

    @Override
    public int getItemCount() {
        return (mResponses == null) ? 0 : mResponses.size();
    }


    public boolean updateData(ArrayList<Reply> responses) {
        mResponses.clear();
        mResponses.addAll(responses);
        notifyDataSetChanged();
        return responses != null;
    }

    public void removeItem(int deletedIndex) {
        mResponses.remove(deletedIndex);
        notifyItemRemoved(deletedIndex);
    }

    public void restoreItem(Reply deletedItem, int deletedIndex) {
        mResponses.add(deletedIndex, deletedItem);
        notifyItemInserted(deletedIndex);
    }

    class ResponseViewHolder extends DoubleLayerViewHolder {
        @BindView(R.id.response_time_textView)
        TextView mTime;
        @BindView(R.id.response_textView)
        TextView mResponse;
        @BindView(R.id.replied_by_textView)
        TextView mAuthor;
        @BindView(R.id.reply_specialty_textView)
        TextView mAuthorSpecialty;
        @BindView(R.id.reply_avatar)
        ImageView mAuthorAvatar;

        ResponseViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            foreground = v.findViewById(R.id.response_foreground_card);
            //tODO see author profile dialog
            mAuthorAvatar.setOnClickListener(view -> {
            });
        }
    }
}
