package com.nexus8international.android.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class DoubleLayerViewHolder extends RecyclerView.ViewHolder {
    public View foreground;

    public DoubleLayerViewHolder(View itemView) {
        super(itemView);
    }
}
