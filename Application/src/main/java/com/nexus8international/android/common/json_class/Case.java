package com.nexus8international.android.common.json_class;

@SuppressWarnings("unused")
public class Case extends HippraPost {
    private String title;
    private int medical_speciality_id;
    private String description;
    private String response_needed;
    private int sub_medical_speciality_id;
    private String medical_speciality_name;
    private String sub_medical_speciality_name;
    private String patient_age;
    private String gender;
    private String race;
    private String ethnicity;
    private String lab_values;
    private String current_stages_of_disease;
    private String current_treatment_administered;
    private String treatment_outcomes;
    private boolean is_deleted;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUserId() {
        return this.user_id;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public int getMedicalSpecialityId() {
        return this.medical_speciality_id;
    }

    public void setMedicalSpecialityId(int medical_speciality_id) {
        this.medical_speciality_id = medical_speciality_id;
    }

    public int getSubMedicalSpecialityId() {
        return this.sub_medical_speciality_id;
    }

    public void setSubMedicalSpecialityId(int sub_medical_speciality_id) {
        this.sub_medical_speciality_id = sub_medical_speciality_id;
    }

    public String getMedicalSpecialityName() {
        return this.medical_speciality_name;
    }

    public void setMedicalSpecialityName(String medical_speciality_name) {
        this.medical_speciality_name = medical_speciality_name;
    }

    public String getSubMedicalSpecialityName() {
        return this.sub_medical_speciality_name;
    }

    public void setSubMedicalSpecialityName(String sub_medical_speciality_name) {
        this.sub_medical_speciality_name = sub_medical_speciality_name;
    }

    public boolean getVisible() {
        return this.visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getPatientAge() {
        return this.patient_age;
    }

    public void setPatientAge(String patient_age) {
        this.patient_age = patient_age;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRace() {
        return this.race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getEthnicity() {
        return this.ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getLabValues() {
        return this.lab_values;
    }

    public void setLabValues(String lab_values) {
        this.lab_values = lab_values;
    }

    public String getCurrentStagesOfDisease() {
        return this.current_stages_of_disease;
    }

    public void setCurrentStagesOfDisease(String current_stages_of_disease) {
        this.current_stages_of_disease = current_stages_of_disease;
    }

    public String getCurrentTreatmentAdministered() {
        return this.current_treatment_administered;
    }

    public void setCurrentTreatmentAdministered(String current_treatment_administered) {
        this.current_treatment_administered = current_treatment_administered;
    }

    public String getTreatmentOutcomes() {
        return this.treatment_outcomes;
    }

    public void setTreatmentOutcomes(String treatment_outcomes) {
        this.treatment_outcomes = treatment_outcomes;
    }

    public boolean getIsDeleted() {
        return this.is_deleted;
    }

    public void setIsDeleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getMedical_speciality_name() {
        return medical_speciality_name;
    }

    public void setMedical_speciality_name(String medical_speciality_name) {
        this.medical_speciality_name = medical_speciality_name;
    }

    public String getResponse_needed() {

        return response_needed;
    }

    public void setResponse_needed(String response_needed) {
        this.response_needed = response_needed;
    }

    public int getMedical_speciality_id() {
        return medical_speciality_id;
    }

    public void setMedical_speciality_id(int medical_speciality_id) {
        this.medical_speciality_id = medical_speciality_id;
    }

    public int getSub_medical_speciality_id() {
        return sub_medical_speciality_id;
    }

    public void setSub_medical_speciality_id(int sub_medical_speciality_id) {
        this.sub_medical_speciality_id = sub_medical_speciality_id;
    }

    public String getSub_medical_speciality_name() {
        return sub_medical_speciality_name;
    }

    public void setSub_medical_speciality_name(String sub_medical_speciality_name) {
        this.sub_medical_speciality_name = sub_medical_speciality_name;
    }

    public String getPatient_age() {
        return patient_age;
    }

    public void setPatient_age(String patient_age) {
        this.patient_age = patient_age;
    }

    public String getLab_values() {
        return lab_values;
    }

    public void setLab_values(String lab_values) {
        this.lab_values = lab_values;
    }

    public String getCurrent_stages_of_disease() {
        return current_stages_of_disease;
    }

    public void setCurrent_stages_of_disease(String current_stages_of_disease) {
        this.current_stages_of_disease = current_stages_of_disease;
    }

    public String getCurrent_treatment_administered() {
        return current_treatment_administered;
    }

    public void setCurrent_treatment_administered(String current_treatment_administered) {
        this.current_treatment_administered = current_treatment_administered;
    }

    public String getTreatment_outcomes() {
        return treatment_outcomes;
    }

    public void setTreatment_outcomes(String treatment_outcomes) {
        this.treatment_outcomes = treatment_outcomes;
    }

    public boolean isIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }
}
