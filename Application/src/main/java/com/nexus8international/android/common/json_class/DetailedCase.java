package com.nexus8international.android.common.json_class;

@SuppressWarnings("unused")
public class DetailedCase extends Case {

    private String first_name;
    private String last_name;
    private String medical_speciality;
    private boolean american_board_certified;
    private String residency_hospital;

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMedical_speciality() {
        return medical_speciality;
    }

    public void setMedical_speciality(String medical_speciality) {
        this.medical_speciality = medical_speciality;
    }

    public boolean isAmerican_board_certified() {
        return american_board_certified;
    }

    public void setAmerican_board_certified(boolean american_board_certified) {
        this.american_board_certified = american_board_certified;
    }

    public String getResidency_hospital() {
        return residency_hospital;
    }

    public void setResidency_hospital(String residency_hospital) {
        this.residency_hospital = residency_hospital;
    }
}
