package com.nexus8international.android.common.json_class;

public class DetailedCaseWithLogin extends DetailedCase {
    private String login;
    private String device_type;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }
}
