package com.nexus8international.android.common.json_class

import java.util.*

class DetailedEntry {
    var case: DetailedCase? = null
    var responses: ArrayList<Reply>? = null
}
/**
 * The original java code is as follows, this doesn't work because the json field name is "case", which
 * is a keyword in java, can use gson's @SerializedName("case") annotation to fix it
 */
//package com.nexus8international.android.common.utils;
//
//import java.util.ArrayList;
//
//public class DetailedEntry {
//    private DetailedCase case;
//
//    public DetailedCase getCase() { return this.case; }
//
//    public void setCase(DetailedCase detailedCase) { this.case = detailedCase; }
//
//    private ArrayList<Reply> responses;
//
//    public ArrayList<Reply> getResponses() { return this.responses; }
//
//    public void setResponses(ArrayList<Reply> responses) { this.responses = responses; }
//}