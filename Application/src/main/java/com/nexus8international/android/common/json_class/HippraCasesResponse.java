package com.nexus8international.android.common.json_class;

import java.util.ArrayList;

public class HippraCasesResponse extends HippraBaseResponse {
    private ArrayList<Case> data = null;

    public ArrayList<Case> getData() {
        return this.data;
    }

    public void setData(ArrayList<Case> data) {
        this.data = data;
    }
}
