package com.nexus8international.android.common.json_class;

public class HippraDetailedCaseResponse extends HippraBaseResponse {

    private DetailedEntry data;

    public DetailedEntry getData() {
        return this.data;
    }

    public void setData(DetailedEntry data) {
        this.data = data;
    }
}
