package com.nexus8international.android.common.json_class;

import android.support.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("unused")
abstract class HippraPost {

    private static SimpleDateFormat formatter;
    private static SimpleDateFormat displayFormatter;
    protected boolean visible;
    int id;
    int user_id;
    private Date created_at;
    private Date updated_at;

    @Nullable
    private static Date parseFormat(String created_at) {
        if (formatter == null) formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        Date date = null;
        try {
            date = formatter.parse(created_at.replaceFirst("\\.(.*)$", "+0000"));

            //This shows how to get the correct time in local time
//            System.out.println(date);
//
//            System.out.println("time zone : " + TimeZone.getDefault().getID());
//            System.out.println(formatter.format(date));

        } catch (ParseException e) {
            //Log.e("Parsing Case Json", "formatting failed");
        }
        return date;
    }

    public static SimpleDateFormat getFormatter() {
        return formatter;
    }

    public static void setFormatter(SimpleDateFormat formatter) {
        HippraPost.formatter = formatter;
    }

    public static SimpleDateFormat getDisplayFormatter() {
        return displayFormatter;
    }

    public static void setDisplayFormatter(SimpleDateFormat displayFormatter) {
        HippraPost.displayFormatter = displayFormatter;
    }

    public String getCreated_at() {
        if (displayFormatter == null)
            displayFormatter = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
        return this.created_at == null ? null : displayFormatter.format(this.created_at);
    }

    public void setCreated_at(String created_at) {
        this.created_at = parseFormat(created_at);
    }

    public String getUpdated_at() {
        if (displayFormatter == null)
            displayFormatter = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
        return this.updated_at == null ? null : displayFormatter.format(this.updated_at);
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = parseFormat(updated_at);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
