package com.nexus8international.android.common.json_class;

public class ReplyWithLogin {
    private String login;
    private String device_type;
    private int case_id;
    private String value;

    public ReplyWithLogin(String username, String type) {
        login = username;
        device_type = type;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public int getCase_id() {
        return case_id;
    }

    public void setCase_id(int case_id) {
        this.case_id = case_id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
