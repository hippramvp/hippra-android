package com.nexus8international.android.common.json_class;

public class User {

    private static final String[] FILTERS = {"Topic", "ResponseNeeded", "Category"};
    private String login;
    private String device_type;
    private String filter_by;
    private String filter1;

    private String filter2;

    public User(String login, String device_type) {
        this.login = login;
        this.device_type = device_type;
    }

    public void setFilter_by(String filter_by) {
        this.filter_by = filter_by;
    }

    public void setFilter1(String filter1) {
        this.filter1 = filter1;
    }

    public void setFilter2(String filter2) {
        this.filter2 = filter2;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public void setDeviceType(String device_type) {
        this.device_type = device_type;
    }

    public void setFilterAndValue(int filter, String value) {
        if (filter > -1) {
            this.filter_by = FILTERS[filter];
            this.filter1 = value;
        }
    }

    public void setFilterAndValue(int filter, String value1, String value2) {
        if (filter > -1) {
            setFilterAndValue(filter, value1);
            this.filter2 = value2;
        }
    }
}
