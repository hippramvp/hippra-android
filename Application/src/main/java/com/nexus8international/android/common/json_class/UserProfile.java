package com.nexus8international.android.common.json_class;

public class UserProfile {

    private String email;
    private String login;
    private String password;
    private String device_type;
    private String device_token;
    private String password_confirmation;
    private String npi_number;
    private String medical_speciality;
    private boolean american_board_certified;
    private String residency_hospital;
    private String first_name;
    private String last_name;
    private String address_1;
    private String address_2;
    private String city;
    private String zip_code;
    private String country;
    private String contact;
    private String education;
    private String state;
    private String medical_school_attended;

    public UserProfile() {
        setDeviceType("iOS");
        setDeviceToken("ASAF-FRED-JKLE-123H");
        setAddress2("");
        setCountry("United States");
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceType() {
        return this.device_type;
    }

    public void setDeviceType(String device_type) {
        this.device_type = device_type;
    }

    public String getDeviceToken() {
        return this.device_token;
    }

    public void setDeviceToken(String device_token) {
        this.device_token = device_token;
    }

    public String getPasswordConfirmation() {
        return this.password_confirmation;
    }

    public void setPasswordConfirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    public String getNpiNumber() {
        return this.npi_number;
    }

    public void setNpiNumber(String npi_number) {
        this.npi_number = npi_number;
    }

    public String getMedicalSpeciality() {
        return this.medical_speciality;
    }

    public void setMedicalSpeciality(String medical_speciality) {
        this.medical_speciality = medical_speciality;
    }

    public boolean getAmericanBoardCertified() {
        return this.american_board_certified;
    }

    public void setAmericanBoardCertified(boolean american_board_certified) {
        this.american_board_certified = american_board_certified;
    }

    public String getResidencyHospital() {
        return this.residency_hospital;
    }

    public void setResidencyHospital(String residency_hospital) {
        this.residency_hospital = residency_hospital;
    }

    public String getFirstName() {
        return this.first_name;
    }

    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public String getLastName() {
        return this.last_name;
    }

    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    public String getAddress1() {
        return this.address_1;
    }

    public void setAddress1(String address_1) {
        this.address_1 = address_1;
    }

    public String getAddress2() {
        return this.address_2;
    }

    public void setAddress2(String address_2) {
        this.address_2 = address_2;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return this.zip_code;
    }

    public void setZipCode(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getContact() {
        return this.contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEducation() {
        return this.education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMedicalSchoolAttended() {
        return this.medical_school_attended;
    }

    public void setMedicalSchoolAttended(String medical_school_attended) {
        this.medical_school_attended = medical_school_attended;
    }
}

