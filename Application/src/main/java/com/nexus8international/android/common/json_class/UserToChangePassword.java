package com.nexus8international.android.common.json_class;

public class UserToChangePassword extends UserWithPassword {

	private String new_password;

	public UserToChangePassword(String username, String password, String iOS, String newPassword) {
		super(username, password, iOS);
		new_password = newPassword;
	}

	public String getNew_password() {
		return new_password;
	}

	public void setNew_password(String new_password) {
		this.new_password = new_password;
	}
}
