package com.nexus8international.android.common.json_class;

public class UserWithPassword extends User {
    private String password;

    public UserWithPassword(String s, String password, String iOS) {
        super(s, iOS);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
