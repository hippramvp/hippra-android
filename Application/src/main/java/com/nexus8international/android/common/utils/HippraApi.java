package com.nexus8international.android.common.utils;

import com.nexus8international.android.common.json_class.DetailedCaseWithLogin;
import com.nexus8international.android.common.json_class.HippraBaseResponse;
import com.nexus8international.android.common.json_class.HippraCasesResponse;
import com.nexus8international.android.common.json_class.HippraDetailedCaseResponse;
import com.nexus8international.android.common.json_class.ReplyWithLogin;
import com.nexus8international.android.common.json_class.User;
import com.nexus8international.android.common.json_class.UserProfile;
import com.nexus8international.android.common.json_class.UserToChangePassword;
import com.nexus8international.android.common.json_class.UserWithPassword;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface HippraApi {
    String CASE_ID_FLAG = "CASE_ID";
    int MODE_FLAG_ME = 1;
    int MODE_FLAG_ALL = 2;
    int REQUEST_SUCCESS = 1;

	int REQUEST_FAIL = 0;
    int DUPLICATE_USERNAME = 2;
    String MODE_KEY = "MODE";
    String FILTER_KEY = "FILTER";
    int FILTER_FLAG_TERM = 0;
    String FILTER_VALUE = "VALUE";

    @PUT("cases/list")
    Call<HippraCasesResponse> listCases(@Body User user);

    @PUT("cases/list/me")
    Call<HippraCasesResponse> myCases(@Body User user);

    @PUT("responses/create")
    Call<HippraBaseResponse> reply(@Body ReplyWithLogin replyWithLogin);

    @PUT("cases/create")
    Call<HippraCasesResponse> postNewCase(@Body DetailedCaseWithLogin caseWithLogin);

    @PUT("users/change_password")
    Call<HippraBaseResponse> changePassword(@Body UserToChangePassword userToChangePassword);

    @POST("users/sign_in")
    Call<HippraBaseResponse> login(@Body UserWithPassword userWithPassword);

    @POST("users/reset_password")
    Call<HippraBaseResponse> forgotPassword(@Body User user);

    @POST("users/sign_up")
    Call<HippraBaseResponse> signUp(@Body UserProfile profile);

    @POST("cases/{id}")
    Call<HippraDetailedCaseResponse> oneCase(@Path("id") int id, @Body User user);

    @HTTP(method = "DELETE", path = "users/logout", hasBody = true)
    Call<HippraBaseResponse> signOut(@Body User user);
}
