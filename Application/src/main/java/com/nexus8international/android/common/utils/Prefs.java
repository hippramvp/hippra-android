package com.nexus8international.android.common.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.nexus8international.android.R;
import com.nexus8international.android.common.json_class.UserProfile;
import com.nexus8international.android.common.json_class.UserWithPassword;

public class Prefs {

	public static final String KEY_LOGIN = "login";

	public static final String KEY_IS_LOGGED_IN = "logged_in";

	public static final String KEY_PASSWORD = "password";

	public static final String KEY_DEVICE = "device";

	public static final String KEY_EMAIL = "email";

	public static final String KEY_TOKEN = "token";

	public static final String KEY_NPI = "npi_number";

	public static final String KEY_SPECIALITY = "specialty";

	public static final String KEY_IS_CERTIFIED = "is_certified";

	public static final String KEY_HOSPITAL = "hospital";

	public static final String KEY_FIRST_NAME = "first_name";

	public static final String KEY_LAST_NAME = "last_name";

	public static final String KEY_ADDRESS_1 = "address_1";

	public static final String KEY_ADDRESS_2 = "address_2";

	public static final String KEY_CITY = "city";

	public static final String KEY_ZIP = "zip";

	public static final String KEY_COUNTRY = "country";

	public static final String KEY_CONTACT = "contact";

	public static final String KEY_EDUCATION = "education";

	public static final String KEY_SCHOOL = "school";

	public static final String KEY_STATE = "state";

	private static SharedPreferences preferences = null;

    public static SharedPreferences getInstance(Context context) {
        if (preferences == null) {
            synchronized (Prefs.class) {
                if (preferences == null) {
                    preferences = context.getSharedPreferences(context.getString(R.string.prefs), Context.MODE_PRIVATE);
                }
            }
        }
        return preferences;
    }

    public static boolean isLoggedIn() {
        return (preferences != null) && preferences.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public static String getUserName() {
        return preferences.getString(KEY_LOGIN, "0");
    }

    public static String getPassword() {
        return preferences.getString(KEY_PASSWORD, "0");
    }

    public static String getDeviceType() {
        return preferences.getString(KEY_DEVICE, "0");
    }


	public static void clear() {
		if (preferences != null) {
			SharedPreferences.Editor editor = preferences.edit();
			editor.remove(KEY_IS_LOGGED_IN);
			editor.remove(KEY_PASSWORD);
			editor.remove(KEY_LOGIN);
			editor.apply();
		}
	}

	public static void setUserAndPassword(UserWithPassword user) {
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(KEY_DEVICE, user.getDevice_type());
		editor.putString(KEY_LOGIN, user.getLogin());
		editor.putString(KEY_PASSWORD, user.getPassword());
		editor.putBoolean(KEY_IS_LOGGED_IN, true);
		editor.apply();
	}

	public static void setUserFullProfile(UserProfile userProfile) {
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(KEY_DEVICE, userProfile.getDeviceType());
		editor.putString(KEY_LOGIN, userProfile.getLogin());
		editor.putString(KEY_PASSWORD, userProfile.getPassword());
		editor.putBoolean(KEY_IS_LOGGED_IN, false);
		editor.putString(KEY_EMAIL, userProfile.getEmail());
		editor.putString(KEY_TOKEN, userProfile.getDeviceToken());
		editor.putString(KEY_NPI, userProfile.getNpiNumber());
		editor.putString(KEY_SPECIALITY, userProfile.getMedicalSpeciality());
		editor.putBoolean(KEY_IS_CERTIFIED, userProfile.getAmericanBoardCertified());
		editor.putString(KEY_HOSPITAL, userProfile.getResidencyHospital());
		editor.putString(KEY_FIRST_NAME, userProfile.getFirstName());
		editor.putString(KEY_LAST_NAME, userProfile.getLastName());
		editor.putString(KEY_ADDRESS_1, userProfile.getAddress1());
		editor.putString(KEY_ADDRESS_2, userProfile.getAddress2());
		editor.putString(KEY_CITY, userProfile.getCity());
		editor.putString(KEY_ZIP, userProfile.getZipCode());
		editor.putString(KEY_COUNTRY, userProfile.getCountry());
		editor.putString(KEY_CONTACT, userProfile.getContact());
		editor.putString(KEY_EDUCATION, userProfile.getEducation());
		editor.putString(KEY_SCHOOL, userProfile.getMedicalSchoolAttended());
		editor.putString(KEY_STATE, userProfile.getState());
		editor.apply();
	}

	public static void setPassword(String password) {
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(KEY_PASSWORD, password);
		editor.apply();
	}
}
