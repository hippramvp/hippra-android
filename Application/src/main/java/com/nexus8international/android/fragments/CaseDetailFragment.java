package com.nexus8international.android.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nexus8international.android.R;
import com.nexus8international.android.common.json_class.DetailedCase;
import com.nexus8international.android.common.json_class.DetailedEntry;
import com.nexus8international.android.common.json_class.HippraDetailedCaseResponse;
import com.nexus8international.android.common.json_class.User;
import com.nexus8international.android.common.utils.HippraApi;
import com.nexus8international.android.common.utils.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CaseDetailFragment extends Fragment {

    private static int mCaseID;
    @BindView(R.id.detail_topic_textView)
    TextView mTopic;
    @BindView(R.id.detail_post_by_textView)
    TextView mAuthor;
    @BindView(R.id.detail_spec_textView)
    TextView mAuthorSpec;
    @BindView(R.id.detail_category_textView)
    TextView mCategory;
    @BindView(R.id.detail_subcategory_textView)
    TextView mSubCategory;
    @BindView(R.id.detail_description_textView)
    TextView mDesc;

    @BindView(R.id.detail_age_textView)
    TextView mAge;
    @BindView(R.id.detail_race_textView)
    TextView mRace;
    @BindView(R.id.detail_ethnicity_textView)
    TextView mEthnicity;
    @BindView(R.id.detail_test_value_textView)
    TextView mTestValue;
    @BindView(R.id.detail_current_stage_textView)
    TextView mCurrentStage;
    @BindView(R.id.detail_gender_textView)
    TextView mGender;
    @BindView(R.id.detail_treatment_textView)
    TextView mTreatment;
    @BindView(R.id.detail_outcome_textView)
    TextView mOutcome;
    @BindView(R.id.detail_date_textView)
    TextView mDateTime;

    private User mUser;

    public CaseDetailFragment() {
        // Required empty public constructor
    }

    public static CaseDetailFragment newInstance(int caseID) {
        CaseDetailFragment fragment = new CaseDetailFragment();
        mCaseID = caseID;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUser = new User(Prefs.getUserName(), Prefs.getDeviceType());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_case_detail, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mCaseID > -1) sendNetworkRequest(mCaseID);
    }

    /**
     * request the case
     *
     * @param id the id of the case
     */
    private void sendNetworkRequest(int id) {
        //create retrofit instance
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.hippra.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        HippraApi hippra = retrofit.create(HippraApi.class);
        Call<HippraDetailedCaseResponse> call = hippra.oneCase(id, mUser);

        call.enqueue(new Callback<HippraDetailedCaseResponse>() {
            @Override
            public void onResponse(Call<HippraDetailedCaseResponse> call, Response<HippraDetailedCaseResponse> response) {
                if (response.isSuccessful()) {
                    //Log.d("----case listed", "request success");
                    DetailedEntry entry = response.body().getData();
                    CaseDetailFragment.this.populateCaseView(entry.getCase());
                }
            }

            @Override
            public void onFailure(Call<HippraDetailedCaseResponse> call, Throwable t) {
                Toast.makeText(getContext(), "detailed case request failed", Toast.LENGTH_LONG).show();
            }

        });
    }

    private void populateCaseView(DetailedCase aCase) {
        if (aCase == null) return;
        //Log.d("Populate case view", aCase.getTitle());
        mTopic.setText(aCase.getTitle());
        String author = aCase.getFirst_name() + " " + aCase.getLast_name();
        mAuthor.setText(author);
        mAuthorSpec.setText(aCase.getMedical_speciality());
        mCategory.setText(aCase.getMedical_speciality());
        mSubCategory.setText(aCase.getSub_medical_speciality_name());
        mDesc.setText(aCase.getDescription());
        mAge.setText(aCase.getPatient_age());
        mRace.setText(aCase.getRace());
        mEthnicity.setText(aCase.getEthnicity());
        mTestValue.setText(aCase.getLab_values());
        mCurrentStage.setText(aCase.getCurrent_stages_of_disease());
        mGender.setText(aCase.getGender());
        mTreatment.setText(aCase.getCurrent_treatment_administered());
        mOutcome.setText(aCase.getTreatment_outcomes());
        mDateTime.setText(aCase.getCreated_at());
    }

    public String getTopic() {
        return mTopic.getText().toString();
    }
}
