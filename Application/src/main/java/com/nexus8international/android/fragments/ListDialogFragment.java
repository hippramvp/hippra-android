package com.nexus8international.android.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nexus8international.android.R;

public class ListDialogFragment extends DialogFragment {

    public static final int RESPONSE_LEVEL_MODE = 0;
    private static final int RACE_MODE = 1;
    private static final int GENDER_MODE = 2;
    private static final int ETHNICITY_MODE = 3;
    public static final int CATEGORY_MODE = 4;

    public static final int SUB_CATEGORY0_MODE = 5;

    public static final int SUB_CATEGORY1_MODE = 6;

    public static final int SUB_CATEGORY2_MODE = 7;

    public static final int[] SUB_CATEGORY_MODES = {5, 6, 7};

    public static final int FILTER_SELECTION_MODE = 8;

    public static final int SETTINGS_SELECTION_MODE = 9;
    static OnDialogItemSelectedListener mListener;
    String mTitle;
    int mMode;
    public ListDialogFragment() {
        // Required empty public constructor
    }

    public static ListDialogFragment newInstance(int mode, OnDialogItemSelectedListener listener) {
        ListDialogFragment fragment = new ListDialogFragment();
        fragment.mMode = mode;
        mListener = listener;
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String[] strs = null;
        switch (mMode) {
            case GENDER_MODE:
                strs = getResources().getStringArray(R.array.genders);
                break;
            case RACE_MODE:
                strs = getResources().getStringArray(R.array.races);
                break;
            case ETHNICITY_MODE:
                strs = getResources().getStringArray(R.array.ethnicities);
                break;
            case CATEGORY_MODE:
                strs = getResources().getStringArray(R.array.categories);
                break;
            case SUB_CATEGORY0_MODE:
                strs = getResources().getStringArray(R.array.sub_category_0);
                break;
            case SUB_CATEGORY1_MODE:
                strs = getResources().getStringArray(R.array.sub_category_1);
                break;
            case SUB_CATEGORY2_MODE:
                strs = getResources().getStringArray(R.array.sub_category_2);
                break;
            case RESPONSE_LEVEL_MODE:
                strs = getResources().getStringArray(R.array.response_levels);
                break;
            case FILTER_SELECTION_MODE:
                strs = getResources().getStringArray(R.array.filter_choices);
                break;
            case SETTINGS_SELECTION_MODE:
                strs = getResources().getStringArray(R.array.setting_options);
            default:
                break;
        }
        builder.setItems(strs, (dialog, which) -> mListener.onSelected(mMode, which));
        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setTitle(getTag());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public interface OnDialogItemSelectedListener {
        void onSelected(int mode, int pos);
    }
}

