package com.nexus8international.android.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.nexus8international.android.R;
import com.nexus8international.android.adapters.ResponseAdapter;
import com.nexus8international.android.callbacks.RecyclerItemSwipeCallback;
import com.nexus8international.android.common.json_class.DetailedEntry;
import com.nexus8international.android.common.json_class.HippraDetailedCaseResponse;
import com.nexus8international.android.common.json_class.Reply;
import com.nexus8international.android.common.json_class.User;
import com.nexus8international.android.common.utils.HippraApi;
import com.nexus8international.android.common.utils.Prefs;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListResponseFragment extends Fragment implements RecyclerItemSwipeCallback.ItemTouchHelperListener {

    OnListResponseListener mListener;
    User mUser;
    int mCaseID;
    @BindView(R.id.response_recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.response_list_frame_layout)
    FrameLayout mFrameLayout;
    private ResponseAdapter mAdapter;

    public ListResponseFragment() {
        // Required empty public constructor
    }

    public static ListResponseFragment newInstance(OnListResponseListener listener, int id) {
        ListResponseFragment fragment = new ListResponseFragment();
        fragment.mListener = listener;
        fragment.mCaseID = id;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new ResponseAdapter();
        mUser = new User(Prefs.getUserName(), Prefs.getDeviceType());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_response, container, false);
        ButterKnife.bind(this, view);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(Objects.requireNonNull(getActivity()),
                DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.
                getDrawable(getActivity(), R.drawable.divider)));
        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //instantiate a callback, which will get the reference of this fragment;
        // then use this callback to create an ItemTouchHelper
        //finally attach this helper to recycler view
        RecyclerItemSwipeCallback callback = new RecyclerItemSwipeCallback(
                0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(callback).attachToRecyclerView(mRecyclerView);
    }

    public void updateResponseList() {
        //create retrofit instance
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.hippra.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        HippraApi hippra = retrofit.create(HippraApi.class);
        Call<HippraDetailedCaseResponse> call = hippra.oneCase(mCaseID, mUser);

        call.enqueue(new Callback<HippraDetailedCaseResponse>() {
            @Override
            public void onResponse(Call<HippraDetailedCaseResponse> call, Response<HippraDetailedCaseResponse> response) {
                if (response.isSuccessful()) {
                    //Log.d("----response list", "response list request success");
                    DetailedEntry entry = response.body().getData();
                    mAdapter.updateData(entry.getResponses());
                }
            }

            @Override
            public void onFailure(Call<HippraDetailedCaseResponse> call, Throwable t) {
                Toast.makeText(getContext(), "response list request failed", Toast.LENGTH_LONG).show();
            }

        });
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        //this overrides the method in the listener interface defined in RecyclerItemSwipeCallback
        ArrayList<Reply> responses = mAdapter.getResponses();
        // get the removed item name to display it in snack bar
        String name = responses.get(viewHolder.getAdapterPosition()).getFirst_name();

        // backup of removed item for undo purpose
        final int deletedIndex = viewHolder.getAdapterPosition();
        final Reply deletedItem = responses.get(deletedIndex);

        // remove the item from recycler view
        mAdapter.removeItem(deletedIndex);

        // showing snack bar with Undo option
        Snackbar snackbar = Snackbar
                .make(mFrameLayout, name + "' response is archived!", Snackbar.LENGTH_LONG);
        snackbar.setAction("UNDO", view -> mAdapter.restoreItem(deletedItem, deletedIndex));
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();
    }

    public interface OnListResponseListener {
        void onUpdateResponseList();
    }
}
