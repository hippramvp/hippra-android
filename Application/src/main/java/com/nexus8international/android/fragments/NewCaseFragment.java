package com.nexus8international.android.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nexus8international.android.R;
import com.nexus8international.android.common.json_class.DetailedCaseWithLogin;
import com.nexus8international.android.common.json_class.HippraCasesResponse;
import com.nexus8international.android.common.json_class.User;
import com.nexus8international.android.common.utils.HippraApi;
import com.nexus8international.android.common.utils.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewCaseFragment extends Fragment implements ListDialogFragment.OnDialogItemSelectedListener {

    private static final int RESPONSE_LEVEL_MODE = 0;
    private static final int RACE_MODE = 1;
    private static final int GENDER_MODE = 2;
    private static final int ETHNICITY_MODE = 3;
    private static final int CATEGORY_MODE = 4;
    private static final int SUB_CATEGORY0_MODE = 5;
    private static final int SUB_CATEGORY1_MODE = 6;
    private static final int SUB_CATEGORY2_MODE = 7;
    User mUser;
    ListDialogFragment mDialogFragment;
    @BindView(R.id.post_progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.edit_text_new_topic)
    EditText mTopic;
    @BindView(R.id.edit_text_age)
    EditText mAgeEditText;
    @BindView(R.id.edit_text_lab_value)
    EditText mLabValueEditText;
    @BindView(R.id.edit_text_disease_stage)
    EditText mStageEditText;
    @BindView(R.id.edit_text_treatment)
    EditText mTreatmentEditText;
    @BindView(R.id.edit_text_out_come)
    EditText mOutcomeEditText;
    @BindView(R.id.edit_text_desc)
    EditText mDesc;
    @BindView(R.id.submit_new_btn)
    Button mSubmitBtn;
    @BindView(R.id.category_btn)
    Button mCategoryBtn;
    @BindView(R.id.sub_category_btn)
    Button mSubCatBtn;
    @BindView(R.id.response_level_btn)
    Button mLevelBtn;
    @BindView(R.id.gender_btn)
    Button mGenderBtn;
    @BindView(R.id.race_btn)
    Button mRaceBtn;
    @BindView(R.id.ethnicity_btn)
    Button mEthnicityBtn;
    DetailedCaseWithLogin mDetailedCaseWithLogin;
    private int mCategory = -1;

    public NewCaseFragment() {
        // Required empty public constructor
        mDetailedCaseWithLogin = new DetailedCaseWithLogin();
        mUser = new User(Prefs.getUserName(), Prefs.getDeviceType());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_new_case, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @OnClick(R.id.gender_btn)
    void selectGender() {
        mDialogFragment = ListDialogFragment.newInstance(GENDER_MODE, this);
        mDialogFragment.show(getActivity().getSupportFragmentManager(), getResources().getString(R.string.gender));
    }

    @OnClick(R.id.ethnicity_btn)
    void selectEthnicity() {
        mDialogFragment = ListDialogFragment.newInstance(ETHNICITY_MODE, this);
        mDialogFragment.show(getActivity().getSupportFragmentManager(), getResources().getString(R.string.ethnicity));
    }

    @OnClick(R.id.race_btn)
    void selectRace() {
        mDialogFragment = ListDialogFragment.newInstance(RACE_MODE, this);
        mDialogFragment.show(getActivity().getSupportFragmentManager(), getResources().getString(R.string.race));
    }

    @OnClick(R.id.response_level_btn)
    void selectResponseNeeded() {
        mDialogFragment = ListDialogFragment.newInstance(RESPONSE_LEVEL_MODE, this);
        mDialogFragment.show(getActivity().getSupportFragmentManager(), getResources().getString(R.string.response_level));
    }

    @OnClick(R.id.category_btn)
    void selectCategory() {
        mDialogFragment = ListDialogFragment.newInstance(CATEGORY_MODE, this);
        mDialogFragment.show(getActivity().getSupportFragmentManager(), getResources().getString(R.string.category));
    }

    @OnClick(R.id.sub_category_btn)
    void selectSubCategory() {
        if (mCategory < 0) {
            Toast.makeText(getActivity(), "Please select category first.", Toast.LENGTH_LONG).show();
            return;
        }
        switch (mCategory) {
            case 0:
                mDialogFragment = ListDialogFragment.newInstance(SUB_CATEGORY0_MODE, this);
                break;
            case 1:
                mDialogFragment = ListDialogFragment.newInstance(SUB_CATEGORY1_MODE, this);
                break;
            case 2:
                mDialogFragment = ListDialogFragment.newInstance(SUB_CATEGORY2_MODE, this);
                break;
            default:
                Toast.makeText(getActivity(), "Please select category first.", Toast.LENGTH_LONG).show();
                break;
        }
        if (mDialogFragment != null) mDialogFragment.show(getActivity().getSupportFragmentManager(),
                getResources().getString(R.string.sub_category));
    }

    @OnClick(R.id.submit_new_btn)
    void submitCase() {
        mDetailedCaseWithLogin.setTitle(mTopic.getText().toString().trim());
        if (mDetailedCaseWithLogin.getTitle() == null || mDetailedCaseWithLogin.getTitle().equals("")) {
            Toast.makeText(getContext(), "please provide the topic", Toast.LENGTH_SHORT).show();
            return;
        }
        mDetailedCaseWithLogin.setDescription(mDesc.getText().toString().trim());
        if (mDetailedCaseWithLogin.getDescription() == null || mDetailedCaseWithLogin.getDescription().equals("")) {
            Toast.makeText(getContext(), "please provide the description", Toast.LENGTH_SHORT).show();
            return;
        }
        if (mDetailedCaseWithLogin.getResponse_needed() == null || mDetailedCaseWithLogin.getMedical_speciality_name() == null
                || mDetailedCaseWithLogin.getSub_medical_speciality_name() == null) {
            Toast.makeText(getContext(), R.string.minimum_case_info_needed, Toast.LENGTH_SHORT).show();
            return;
        }
        mDetailedCaseWithLogin.setLab_values(mLabValueEditText.getText().toString());
        mDetailedCaseWithLogin.setCurrent_stages_of_disease(mStageEditText.getText().toString());
        mDetailedCaseWithLogin.setCurrent_treatment_administered(mTreatmentEditText.getText().toString());
        mDetailedCaseWithLogin.setTreatment_outcomes(mOutcomeEditText.getText().toString());
        mDetailedCaseWithLogin.setPatient_age(mAgeEditText.getText().toString());
        mDetailedCaseWithLogin.setLogin(mUser.getLogin());
        mDetailedCaseWithLogin.setDevice_type(mUser.getDevice_type());
        postNewCase();
    }

    private void postNewCase() {
        mProgressBar.setVisibility(View.VISIBLE);
        //create retrofit instance
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.hippra.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        HippraApi hippra = retrofit.create(HippraApi.class);
        Call<HippraCasesResponse> call = hippra.postNewCase(mDetailedCaseWithLogin);
        call.enqueue(new Callback<HippraCasesResponse>() {
            @Override
            public void onResponse(Call<HippraCasesResponse> call, Response<HippraCasesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null || response.body().getStatus() != 1) {
                        Toast.makeText(getContext(), "not posted, change title", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Your case has been posted.", Toast.LENGTH_SHORT).show();
                        if (getActivity() != null) getActivity().finish();
                    }
                } else
                    Toast.makeText(getContext(), "not posted, change title", Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<HippraCasesResponse> call, Throwable t) {
                Toast.makeText(getContext(), "Please keep the topic short.", Toast.LENGTH_LONG).show();
                mProgressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onSelected(int mode, int pos) {
        String str;
        int id;
        switch (mode) {
            case RESPONSE_LEVEL_MODE:
                str = getResources().getStringArray(R.array.response_levels)[pos];
                mLevelBtn.setText(str);
                mDetailedCaseWithLogin.setResponse_needed(str);
                break;
            case GENDER_MODE:
                str = getResources().getStringArray(R.array.genders)[pos];
                mGenderBtn.setText(str);
                mDetailedCaseWithLogin.setGender(str);
                break;
            case RACE_MODE:
                str = getResources().getStringArray(R.array.races)[pos];
                mRaceBtn.setText(str);
                mDetailedCaseWithLogin.setRace(str);
                break;
            case ETHNICITY_MODE:
                str = getResources().getStringArray(R.array.ethnicities)[pos];
                mEthnicityBtn.setText(str);
                mDetailedCaseWithLogin.setEthnicity(str);
                break;
            case CATEGORY_MODE:
                mCategory = pos;
                str = getResources().getStringArray(R.array.categories)[pos];
                id = getResources().getIntArray(R.array.category_ids)[pos];
                mCategoryBtn.setText(str);
                mDetailedCaseWithLogin.setMedical_speciality_name(str);
                mDetailedCaseWithLogin.setMedical_speciality_id(id);
                //if sub category has a value, reset it
                mSubCatBtn.setText(R.string.select_sub_category);
                mDetailedCaseWithLogin.setSub_medical_speciality_name(null);
                mDetailedCaseWithLogin.setSub_medical_speciality_id(0);
                break;
            case SUB_CATEGORY0_MODE:
                str = getResources().getStringArray(R.array.sub_category_0)[pos];
                id = getResources().getIntArray(R.array.sub_category_0_ids)[pos];
                mSubCatBtn.setText(str);
                mDetailedCaseWithLogin.setSub_medical_speciality_name(str);
                mDetailedCaseWithLogin.setSub_medical_speciality_id(id);
                break;
            case SUB_CATEGORY1_MODE:
                str = getResources().getStringArray(R.array.sub_category_1)[pos];
                id = getResources().getIntArray(R.array.sub_category_1_ids)[pos];
                mSubCatBtn.setText(str);
                mDetailedCaseWithLogin.setSub_medical_speciality_name(str);
                mDetailedCaseWithLogin.setSub_medical_speciality_id(id);
                break;
            case SUB_CATEGORY2_MODE:
                str = getResources().getStringArray(R.array.sub_category_2)[pos];
                id = getResources().getIntArray(R.array.sub_category_2_ids)[pos];
                mSubCatBtn.setText(str);
                mDetailedCaseWithLogin.setSub_medical_speciality_name(str);
                mDetailedCaseWithLogin.setSub_medical_speciality_id(id);
                break;
        }
    }
}
