package com.nexus8international.android.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nexus8international.android.R;
import com.nexus8international.android.activities.BrowseCasesActivity;
import com.nexus8international.android.adapters.CaseAdapter;
import com.nexus8international.android.callbacks.RecyclerItemSwipeCallback;
import com.nexus8international.android.common.json_class.Case;
import com.nexus8international.android.common.json_class.HippraCasesResponse;
import com.nexus8international.android.common.json_class.User;
import com.nexus8international.android.common.utils.HippraApi;
import com.nexus8international.android.common.utils.Prefs;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecyclerViewFragment extends Fragment implements RecyclerItemSwipeCallback.ItemTouchHelperListener {

    private static final String TAG = "RecyclerViewFragment";
    protected CaseAdapter mAdapter;
    @BindView(R.id.cases_frag_linear_layout)
    LinearLayout mLinearLayout;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private User mUser;
    private int mBrowseMode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new CaseAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        //change the title of toolbar
        if (getArguments() != null && getArguments().getInt(HippraApi.MODE_KEY, -1) == HippraApi.MODE_FLAG_ME) {
            BrowseCasesActivity activity = (BrowseCasesActivity) getActivity();
            if (activity != null) activity.setToolbarTitle(R.string.my_cases_str);
        }

        View rootView = inflater.inflate(R.layout.recycler_view_frag, container, false);
        rootView.setTag(TAG);
        ButterKnife.bind(this, rootView);
		DividerItemDecoration itemDecoration = new DividerItemDecoration(Objects.requireNonNull(getActivity()),
				DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.
                    getDrawable(getActivity(), R.drawable.divider)));
        mRecyclerView.addItemDecoration(itemDecoration);
	    mRecyclerView.setAdapter(mAdapter);
		mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		return rootView;
	}

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        mUser = new User(Prefs.getUserName(), Prefs.getDeviceType());
        if (bundle != null) {
            mBrowseMode = bundle.getInt(HippraApi.MODE_KEY, -1);
            mUser.setFilterAndValue(bundle.getInt(HippraApi.FILTER_KEY, -1),
                    bundle.getString(HippraApi.FILTER_VALUE, null));
        }
        sendNetworkRequest();
        //instantiate a callback, which will get the reference of this fragment;
        // then use this callback to create an ItemTouchHelper
        //finally attach this helper to recycler view
        RecyclerItemSwipeCallback callback = new RecyclerItemSwipeCallback(
                0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(callback).attachToRecyclerView(mRecyclerView);
    }

	public void updateFilter(int filterMode, String value1, String value2) {
		mUser.setFilterAndValue(filterMode, value1, value2);
	}

	public void sendNetworkRequest() {
        //create retrofit instance
		Retrofit.Builder builder = new Retrofit.Builder()
				.baseUrl("https://api.hippra.com/api/v1/")
				.addConverterFactory(GsonConverterFactory.create());
		Retrofit retrofit = builder.build();
		HippraApi hippra = retrofit.create(HippraApi.class);
        Call<HippraCasesResponse> call = (mBrowseMode == HippraApi.MODE_FLAG_ALL) ?
                hippra.listCases(mUser) : hippra.myCases(mUser);

		call.enqueue(new Callback<HippraCasesResponse>() {
			@Override
			public void onResponse(Call<HippraCasesResponse> call, Response<HippraCasesResponse> response) {
				if (response.isSuccessful()) {
                    //Log.d("----List cases", "request success");
					mAdapter.updateData(response.body().getData());
				}
			}

			@Override
			public void onFailure(Call<HippraCasesResponse> call, Throwable t) {
				Toast.makeText(getContext(), "failed", Toast.LENGTH_LONG).show();
			}

		});
	}

	public void updateResult(String searchTerm) {
        mUser.setFilterAndValue(HippraApi.FILTER_FLAG_TERM, searchTerm);
        sendNetworkRequest();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        //this overrides the method in the listener interface defined in RecyclerItemSwipeCallback
        ArrayList<Case> dataList = mAdapter.getDataList();
        // get the removed item name to display it in snack bar
        String name = dataList.get(viewHolder.getAdapterPosition()).getTitle();

        // backup of removed item for undo purpose
        final int deletedIndex = viewHolder.getAdapterPosition();
        final Case deletedItem = dataList.get(deletedIndex);

        // remove the item from recycler view
        mAdapter.removeItem(deletedIndex);

        // showing snack bar with Undo option
        Snackbar snackbar = Snackbar
                .make(mLinearLayout, "Case \"" + name + "\" is archived!", Snackbar.LENGTH_LONG);
        snackbar.setAction("UNDO", view -> mAdapter.restoreItem(deletedItem, deletedIndex));
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();
    }
}
