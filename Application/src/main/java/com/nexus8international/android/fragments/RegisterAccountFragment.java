package com.nexus8international.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

import com.nexus8international.android.R;
import com.nexus8international.android.common.json_class.HippraBaseResponse;
import com.nexus8international.android.common.json_class.UserProfile;
import com.nexus8international.android.common.utils.HippraApi;
import com.nexus8international.android.common.utils.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.nexus8international.android.fragments.ListDialogFragment.CATEGORY_MODE;

public class RegisterAccountFragment extends Fragment implements ListDialogFragment.OnDialogItemSelectedListener {
    @BindView(R.id.register_basic_info_layout)
    ViewGroup mBasicInfoLayout;
    @BindView(R.id.register_detailed_info_layout)
    ViewGroup mDetailedInfoLayout;
    @BindView(R.id.register_next_btn)
    Button mNextButton;
    @BindView(R.id.register_specialty_button)
    Button mSpecialtyBtn;
    @BindView(R.id.register_submit_button)
    Button mSignUpButton;
    @BindView(R.id.register_email_edittext)
    EditText mEmailText;
    @BindView(R.id.register_first_name_edittext)
    EditText mFirstName;
    @BindView(R.id.register_last_name_edittext)
    EditText mLastName;
    @BindView(R.id.register_id_edittext)
    EditText mNPID;
    @BindView(R.id.register_username_editText)
    EditText mUsername;
    @BindView(R.id.register_password_edittext)
    EditText mPasswordFirst;
    @BindView(R.id.register_reenter_edittext)
    EditText mPasswordRe;
    OnRegisteredListener mListener;
    UserProfile mUserProfile;

    @BindView(R.id.radio_yes)
    RadioButton mYesRadioBtn;
    @BindView(R.id.register_hospital_editText)
    EditText mHospitalText;
    @BindView(R.id.medical_school_editText)
    EditText mSchoolText;
    @BindView(R.id.degree_editText)
    EditText mDegreeText;
    @BindView(R.id.address_editText)
    EditText mAddressText;
    @BindView(R.id.city_editText)
    EditText mCityText;
    @BindView(R.id.zipcode_editText)
    EditText mZipCodeText;
    @BindView(R.id.state_editText)
    EditText mStateText;
    @BindView(R.id.phone_editText)
    EditText mPhoneText;
    @BindView(R.id.register_progress)
    ProgressBar mProgressBar;
    ListDialogFragment mListDialogFragment;
    private String mSpecialty;

    public RegisterAccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_account, container, false);
        ButterKnife.bind(this, view);
        mNextButton.setOnClickListener(v -> onNextButtonPressed());
        mSpecialtyBtn.setOnClickListener(v ->
        {
            ListDialogFragment.newInstance(CATEGORY_MODE, this).show(
                    getActivity().getSupportFragmentManager(), getResources().getString(R.string.medical_speciality));
        });
        mSignUpButton.setOnClickListener(v -> onSignUpButtonPressed());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mBasicInfoLayout.setVisibility(View.VISIBLE);
        mDetailedInfoLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRegisteredListener) {
            mListener = (OnRegisteredListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void onNextButtonPressed() {
        if (mFirstName.getText().toString().equals("")) {
            Toast.makeText(getContext(), R.string.no_first_name, Toast.LENGTH_SHORT).show();
            return;
        }
        if (mLastName.getText().toString().equals("")) {
            Toast.makeText(getContext(), R.string.no_last_name, Toast.LENGTH_SHORT).show();
            return;
        }
        if (mNPID.getText().toString().equals("")) {
            Toast.makeText(getContext(), R.string.no_npid, Toast.LENGTH_SHORT).show();
            return;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mEmailText.getText().toString()).matches()) {
            Toast.makeText(getContext(), R.string.invalid_email, Toast.LENGTH_SHORT).show();
            return;
        }
        if (mUsername.getText().toString().length() < 3) {
            Toast.makeText(getContext(), R.string.username_too_short, Toast.LENGTH_SHORT).show();
            return;
        }
        if (mPasswordFirst.getText().toString().length() < 8) {
            Toast.makeText(getContext(), R.string.passwords_too_short, Toast.LENGTH_SHORT).show();
            return;
        }
        if (!mPasswordFirst.getText().toString().equals(mPasswordRe.getText().toString())) {
            Toast.makeText(getContext(), R.string.passwords_not_match, Toast.LENGTH_SHORT).show();
            return;
        }
        mUserProfile = new UserProfile();
        mUserProfile.setLogin(mUsername.getText().toString());
        mUserProfile.setPassword(mPasswordFirst.getText().toString());
        mUserProfile.setPasswordConfirmation(mPasswordRe.getText().toString());
        mUserProfile.setEmail(mEmailText.getText().toString());
        mUserProfile.setFirstName(mFirstName.getText().toString());
        mUserProfile.setLastName(mLastName.getText().toString());
        mUserProfile.setNpiNumber(mNPID.getText().toString());

        mBasicInfoLayout.setVisibility(View.INVISIBLE);
        mDetailedInfoLayout.setVisibility(View.VISIBLE);
    }

    private void onSignUpButtonPressed() {
        if (mUserProfile == null) return;
        mProgressBar.setVisibility(View.VISIBLE);
        mUserProfile.setMedicalSpeciality(mSpecialty);
        mUserProfile.setAmericanBoardCertified(mYesRadioBtn.isChecked());
        mUserProfile.setResidencyHospital(mHospitalText.getText().toString());
        mUserProfile.setMedicalSchoolAttended(mSchoolText.getText().toString());
        mUserProfile.setEducation(mDegreeText.getText().toString());
        mUserProfile.setAddress1(mAddressText.getText().toString());
        mUserProfile.setCity(mCityText.getText().toString());
        mUserProfile.setZipCode(mZipCodeText.getText().toString());
        mUserProfile.setState(mStateText.getText().toString());
        mUserProfile.setContact(mPhoneText.getText().toString());
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.hippra.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        HippraApi hippra = retrofit.create(HippraApi.class);
        Call<HippraBaseResponse> call = hippra.signUp(mUserProfile);
        call.enqueue(new Callback<HippraBaseResponse>() {
            @Override
            public void onResponse(Call<HippraBaseResponse> call, Response<HippraBaseResponse> response) {
                mProgressBar.setVisibility(View.INVISIBLE);
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == HippraApi.REQUEST_SUCCESS) {
                        Prefs.setUserFullProfile(mUserProfile);
                        mListener.onRegistered(mUserProfile);
                    } else if (response.body().getStatus() == HippraApi.DUPLICATE_USERNAME) {
                        Toast.makeText(RegisterAccountFragment.this.getContext(), R.string.fail_duplicate_username, Toast.LENGTH_LONG).show();
                    } else {
	                    Toast.makeText(RegisterAccountFragment.this.getContext(), R.string.fail_request_notice, Toast.LENGTH_LONG).show();
                    }
                } else {
	                Toast.makeText(RegisterAccountFragment.this.getContext(), R.string.fail_request_notice, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<HippraBaseResponse> call, Throwable t) {
                mProgressBar.setVisibility(View.INVISIBLE);
	            Toast.makeText(RegisterAccountFragment.this.getContext(), R.string.fail_request_notice, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onSelected(int mode, int pos) {
        switch (mode) {
            case CATEGORY_MODE:
                mSpecialty = getResources().getStringArray(R.array.categories)[pos];
                String str = getResources().getString(R.string.medical_speciality) + mSpecialty;
                mSpecialtyBtn.setText(str);
                break;
            default:
                break;
        }
    }

    public interface OnRegisteredListener {
        void onRegistered(UserProfile profile);
    }
}
