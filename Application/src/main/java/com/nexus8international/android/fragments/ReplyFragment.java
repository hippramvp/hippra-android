package com.nexus8international.android.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.nexus8international.android.R;
import com.nexus8international.android.common.json_class.HippraBaseResponse;
import com.nexus8international.android.common.json_class.ReplyWithLogin;
import com.nexus8international.android.common.utils.HippraApi;
import com.nexus8international.android.common.utils.Prefs;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnReplySentListener} interface
 * to handle interaction events.
 */
public class ReplyFragment extends Fragment {

    int mCaseID;
    @BindView(R.id.reply_text)
    EditText mReplyText;
    private OnReplySentListener mListener;
    public ReplyFragment() {
        // Required empty public constructor
    }

    public static ReplyFragment newInstance(OnReplySentListener listener, int id) {
        ReplyFragment replyFragment = new ReplyFragment();
        replyFragment.mCaseID = id;
        replyFragment.mListener = listener;
        return replyFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reply, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReplySentListener) {
            mListener = (OnReplySentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnReplySentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.reply_img_btn)
    void onReplyButtonPressed() {
        ReplyWithLogin replyWithLogin = new ReplyWithLogin(Prefs.getUserName(), Prefs.getDeviceType());
        replyWithLogin.setCase_id(mCaseID);
        replyWithLogin.setValue(mReplyText.getText().toString());
        clearKeyboardFocus();
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://api.hippra.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        HippraApi hippra = retrofit.create(HippraApi.class);
        Call<HippraBaseResponse> call = hippra.reply(replyWithLogin);
        call.enqueue(new Callback<HippraBaseResponse>() {
            @Override
            public void onResponse(Call<HippraBaseResponse> call, Response<HippraBaseResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus() == 1) {
                    Toast.makeText(getContext(), "Your reply has bee sent successfully.", Toast.LENGTH_SHORT).show();
                    mReplyText.setText(null);
                } else
	                Toast.makeText(getContext(), R.string.fail_request_notice, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<HippraBaseResponse> call, Throwable t) {
	            Toast.makeText(getContext(), R.string.fail_request_notice, Toast.LENGTH_SHORT).show();
            }
        });
        mListener.onReply();
    }

    private void clearKeyboardFocus() {
        mReplyText.clearFocus();
        Activity activity = (Activity) mListener;
        if (activity != null) {
            InputMethodManager imm =
                    (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm == null) return;
            imm.hideSoftInputFromWindow(Objects.requireNonNull(activity.getCurrentFocus()).getWindowToken(), 0);
        }
    }

    public interface OnReplySentListener {
        void onReply();
    }
}
