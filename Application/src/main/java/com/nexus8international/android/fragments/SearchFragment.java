package com.nexus8international.android.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.nexus8international.android.R;
import com.nexus8international.android.common.utils.HippraApi;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnSearchListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {
    @BindView(R.id.search_img_btn)
    ImageButton searchBtn;
    @BindView(R.id.search_term)
    EditText searchText;
    private String mSearchTerm = "topic";
    private String mTerm;
    private OnSearchListener mListener;

    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
//        args.putString(SEARCH_TERM, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTerm = getArguments().getString(mSearchTerm);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_topic_frag, container, false);
        ButterKnife.bind(this, view);
        searchText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEND) {
                clearKeyboardFocus();
                onSearchButtonPressed();
                return true;
            }
            return false;
        });
        if (getArguments() != null)
            mSearchTerm = getArguments().getString(HippraApi.FILTER_VALUE, null);
        searchText.setText(mSearchTerm);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSearchListener) {
            mListener = (OnSearchListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnSearchListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.search_img_btn)
    public void onSearchButtonPressed() {
        if (mListener != null) {
            mTerm = searchText.getText().toString();
            mListener.onSearchByTopic(mTerm);
        }
        clearKeyboardFocus();

    }

    private void clearKeyboardFocus() {
        searchText.clearFocus();
        Activity activity = (Activity) mListener;
        if (activity != null) {
            InputMethodManager imm =
                    (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm == null) return;
            imm.hideSoftInputFromWindow(Objects.requireNonNull(activity.getCurrentFocus()).getWindowToken(), 0);
        }
    }

    public interface OnSearchListener {
        void onSearchByTopic(String searchTerm);
    }
}
