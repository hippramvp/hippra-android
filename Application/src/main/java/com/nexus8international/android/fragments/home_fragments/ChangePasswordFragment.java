package com.nexus8international.android.fragments.home_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nexus8international.android.R;
import com.nexus8international.android.common.json_class.HippraBaseResponse;
import com.nexus8international.android.common.json_class.UserToChangePassword;
import com.nexus8international.android.common.utils.HippraApi;
import com.nexus8international.android.common.utils.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChangePasswordFragment extends Fragment {

	@BindView(R.id.change_password_old_edittext)
	EditText mOldPassword;

	@BindView(R.id.change_password_new_edittext)
	EditText mNewPassword;

	@BindView(R.id.change_password_new_re_edittext)
	EditText mNewRePassword;

	@BindView(R.id.change_password_submit_btn)
	Button mSubmitBtn;

	public ChangePasswordFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_change_password, container, false);
		ButterKnife.bind(this, view);
		return view;
	}

	@OnClick(R.id.change_password_submit_btn)
	void onSubmitBtnPressed() {
		String oldPassword = mOldPassword.getText().toString();
		if (oldPassword.equals("")) {
			Toast.makeText(getContext(), R.string.enter_old_password_str, Toast.LENGTH_SHORT).show();
			return;
		}
		String newPassword = mNewPassword.getText().toString();
		if (newPassword.length() < 8) {
			Toast.makeText(getContext(), R.string.passwords_too_short, Toast.LENGTH_SHORT).show();
			return;
		}
		if (!newPassword.equals(mNewRePassword.getText().toString())) {
			Toast.makeText(getContext(), R.string.passwords_not_match, Toast.LENGTH_SHORT).show();
			return;
		}
		UserToChangePassword user = new UserToChangePassword(
				Prefs.getUserName(), oldPassword, Prefs.getDeviceType(), newPassword);
		//create retrofit instance
		Retrofit.Builder builder = new Retrofit.Builder()
				.baseUrl("https://api.hippra.com/api/v1/")
				.addConverterFactory(GsonConverterFactory.create());
		Retrofit retrofit = builder.build();
		HippraApi hippra = retrofit.create(HippraApi.class);
		Call<HippraBaseResponse> call = hippra.changePassword(user);
		call.enqueue(new Callback<HippraBaseResponse>() {
			@Override
			public void onResponse(Call<HippraBaseResponse> call, Response<HippraBaseResponse> response) {
				if (response.isSuccessful() && response.body().getStatus() == 1) {
					Toast.makeText(getContext(), R.string.password_changed, Toast.LENGTH_SHORT).show();
					//update prefs
					Prefs.setPassword(newPassword);
					ChangePasswordFragment.this.clearInput();
					if (getActivity() != null)
						getActivity().getSupportFragmentManager().popBackStack();
				} else if (response.isSuccessful() && response.body().getStatus() == HippraApi.REQUEST_FAIL) {
					Toast.makeText(getContext(), R.string.invalid_password, Toast.LENGTH_SHORT).show();
				} else
					Toast.makeText(getContext(), R.string.password_not_changed, Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onFailure(Call<HippraBaseResponse> call, Throwable t) {
				Toast.makeText(getContext(), R.string.password_not_changed, Toast.LENGTH_SHORT).show();
			}
		});
	}

	private void clearInput() {
		mOldPassword.setText("");
		mNewPassword.setText("");
		mNewRePassword.setText("");
	}
}
