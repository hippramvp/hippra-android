package com.nexus8international.android.fragments.home_fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.nexus8international.android.R;
import com.nexus8international.android.activities.BrowseCasesActivity;
import com.nexus8international.android.activities.NewCaseActivity;
import com.nexus8international.android.common.utils.HippraApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeScreenFragment extends Fragment {

	private static String searchTerm = null;

	@BindView(R.id.search_term)
	TextView mTextView;

	private OnFragmentInteractionListener mListener;

	public HomeScreenFragment() {
		// Required empty public constructor
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//if the enter key on the keyboard is pressed, start the activity
		//The ime option is mentioned in the xml element of the edit text
		mTextView.setOnEditorActionListener((textView, i, keyEvent) -> {
			if (i == EditorInfo.IME_ACTION_SEND) {
				mTextView.clearFocus();
				startBrowseCasesActivity();
				return true;
			}
			return false;
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_home_scrren, container, false);
		ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnFragmentInteractionListener) {
			mListener = (OnFragmentInteractionListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@OnClick(R.id.post_btn)
	void startNewCaseActivity() {
		startActivity(new Intent(getContext(), NewCaseActivity.class));
        ((Activity) mListener).overridePendingTransition(R.anim.enter, R.anim.exit);
	}

	@OnClick({R.id.browse_btn, R.id.search_img_btn})
	void startBrowseCasesActivity() {
		Intent intent = new Intent(getContext(), BrowseCasesActivity.class);
		intent.putExtra(HippraApi.MODE_KEY, HippraApi.MODE_FLAG_ALL);
		searchTerm = mTextView.getText().toString();
		intent.putExtra(HippraApi.FILTER_KEY, HippraApi.FILTER_FLAG_TERM);
		intent.putExtra(HippraApi.FILTER_VALUE, searchTerm);
		startActivity(intent);
        ((Activity) mListener).overridePendingTransition(R.anim.enter, R.anim.exit);
	}

	@OnClick(R.id.my_case_btn)
	void startMyCasesActivity() {
		Intent intent = new Intent(getContext(), BrowseCasesActivity.class);
		intent.putExtra(HippraApi.MODE_KEY, HippraApi.MODE_FLAG_ME);
		startActivity(intent);
        ((Activity) mListener).overridePendingTransition(R.anim.enter, R.anim.exit);
	}

	@OnClick(R.id.profile_btn)
	void replaceWithProfileFragment() {
		mListener.onProfileBtnPressed();

	}

	public interface OnFragmentInteractionListener {
		void onProfileBtnPressed();
	}
}
