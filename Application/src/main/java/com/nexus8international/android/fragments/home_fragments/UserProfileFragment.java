package com.nexus8international.android.fragments.home_fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.nexus8international.android.R;
import com.nexus8international.android.common.utils.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserProfileFragment extends Fragment {

	@BindView(R.id.profile_first_name_edittext)
	EditText mFirstName;

	@BindView(R.id.profile_last_name_edittext)
	EditText mLastName;

	@BindView(R.id.profile_id_edittext)
	EditText mNPI;

	@BindView(R.id.profile_username_editText)
	EditText mUserName;

	@BindView(R.id.profile_email_edittext)
	EditText mEmail;

	@BindView(R.id.profile_radio_group)
	RadioGroup mRadioGroup;

	@BindView(R.id.profile_radio_yes)
	RadioButton mRadioYes;

	@BindView(R.id.profile_radio_no)
	RadioButton mRadioNo;

	@BindView(R.id.profile_hospital_editText)
	EditText mHospital;

	@BindView(R.id.profile_medical_school_editText)
	EditText mSchool;

	@BindView(R.id.profile_degree_editText)
	EditText mEducationDegree;

	@BindView(R.id.profile_address_editText)
	EditText mAddress;

	@BindView(R.id.profile_city_editText)
	EditText mCity;

	@BindView(R.id.profile_zipcode_editText)
	EditText mZip;

	@BindView(R.id.profile_state_editText)
	EditText mState;

	@BindView(R.id.profile_phone_editText)
	EditText mPhone;

	public UserProfileFragment() {
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
		ButterKnife.bind(this, view);
		SharedPreferences prefs = Prefs.getInstance(getContext());
		mFirstName.setText(prefs.getString(Prefs.KEY_FIRST_NAME, null));
		mFirstName.setFocusable(false);
		mLastName.setText(prefs.getString(Prefs.KEY_LAST_NAME, null));
		mLastName.setFocusable(false);
		mNPI.setText(prefs.getString(Prefs.KEY_NPI, null));
		mNPI.setFocusable(false);
		mUserName.setText(prefs.getString(Prefs.KEY_LOGIN, null));
		mUserName.setFocusable(false);
		mEmail.setText(prefs.getString(Prefs.KEY_EMAIL, null));
		mEmail.setFocusable(false);
		if (prefs.getBoolean(Prefs.KEY_IS_CERTIFIED, false)) {
			mRadioYes.setChecked(true);
			mRadioNo.setChecked(false);
		} else {
			mRadioNo.setChecked(true);
			mRadioYes.setChecked(false);
		}
		mRadioNo.setEnabled(false);
		mRadioYes.setEnabled(false);
		mRadioGroup.setEnabled(false);
		mHospital.setText(prefs.getString(Prefs.KEY_HOSPITAL, null));
		mHospital.setFocusable(false);
		mSchool.setText(prefs.getString(Prefs.KEY_SCHOOL, null));
		mSchool.setFocusable(false);
		mEducationDegree.setText(prefs.getString(Prefs.KEY_EDUCATION, null));
		mEducationDegree.setFocusable(false);
		mAddress.setText(prefs.getString(Prefs.KEY_ADDRESS_1, null));
		mAddress.setFocusable(false);
		mCity.setText(prefs.getString(Prefs.KEY_CITY, null));
		mCity.setFocusable(false);
		mZip.setText(prefs.getString(Prefs.KEY_ZIP, null));
		mZip.setFocusable(false);
		mState.setText(prefs.getString(Prefs.KEY_STATE, null));
		mState.setFocusable(false);
		mPhone.setText(prefs.getString(Prefs.KEY_CONTACT, null));
		mPhone.setFocusable(false);
		return view;
	}

}
