package com.nexus8international.android.fragments.sigin_signup_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.nexus8international.android.R;
import com.nexus8international.android.activities.LoginActivity;
import com.nexus8international.android.common.json_class.HippraBaseResponse;
import com.nexus8international.android.common.json_class.User;
import com.nexus8international.android.common.json_class.UserWithPassword;
import com.nexus8international.android.common.utils.HippraApi;
import com.nexus8international.android.common.utils.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class LoginFragment extends Fragment {

    @BindView(R.id.sign_in_relative_layout)
    RelativeLayout mSignInLayout;
    @BindView(R.id.forgot_password_layout)
    RelativeLayout mForgotPasswordLayout;
    @BindView(R.id.username_reset_edit_text)
    EditText mResetUserNameText;
    @BindView(R.id.submit_reset_btn)
    Button mResetButton;

    @BindView(R.id.submit_sign_in_btn)
    Button mSignInButton;
    @BindView(R.id.forgot_password_btn)
    Button mForgotButton;
    @BindView(R.id.user_name_login_edit_text)
    EditText mUsernameText;
    @BindView(R.id.password_edit_text)
    EditText mPasswordText;
    private OnFragmentInteractionListener mListener;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        mSignInButton.setOnClickListener(v -> onButtonPressed(R.id.submit_sign_in_btn));
        mForgotButton.setOnClickListener(v -> onButtonPressed(R.id.forgot_password_btn));
        mResetButton.setOnClickListener(v -> onButtonPressed(R.id.submit_reset_btn));
        return view;
    }

    public void onButtonPressed(int id) {
        if (mListener == null) return;
        if (id == R.id.submit_sign_in_btn) {
            UserWithPassword user = new UserWithPassword(mUsernameText.getText().toString(), mPasswordText.getText().toString(), "iOS");
            //create retrofit instance
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl("https://api.hippra.com/api/v1/")
                    .addConverterFactory(GsonConverterFactory.create());
            Retrofit retrofit = builder.build();
            HippraApi hippra = retrofit.create(HippraApi.class);
            Call<HippraBaseResponse> call = hippra.login(user);
            call.enqueue(new Callback<HippraBaseResponse>() {
                @Override
                public void onResponse(Call<HippraBaseResponse> call, Response<HippraBaseResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() == HippraApi.REQUEST_SUCCESS) {
	                        Prefs.setUserAndPassword(user);
                            mListener.onFragmentInteraction(id);
                        } else if (response.body().getStatus() == HippraApi.REQUEST_FAIL &&
                                response.body().getMessage().equals(getResources().getString(R.string.not_validated_user))) {
                            //show the dialog
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginFragment.this.getContext());
                            builder.setMessage(R.string.not_validated_account_str).setTitle("Please Bear with Us").setCancelable(false);
                            builder.setPositiveButton(R.string.ok, null);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.PinkFontColor));
                        } else {
                            Toast.makeText(LoginFragment.this.getContext(), R.string.login_fail_str, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(LoginFragment.this.getContext(), R.string.fail_request_notice, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<HippraBaseResponse> call, Throwable t) {
                    Toast.makeText(getContext(), R.string.fail_request_notice, Toast.LENGTH_LONG).show();
                }
            });
        } else if (id == R.id.submit_reset_btn) {
            LoginActivity activity = (LoginActivity) mListener;
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) imm.hideSoftInputFromWindow(mResetUserNameText.getWindowToken(), 0);

            User user = new User(mResetUserNameText.getText().toString(), "iOS");
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl("https://api.hippra.com/api/v1/")
                    .addConverterFactory(GsonConverterFactory.create());
            Retrofit retrofit = builder.build();
            HippraApi hippra = retrofit.create(HippraApi.class);
            Call<HippraBaseResponse> call = hippra.forgotPassword(user);
            call.enqueue(new Callback<HippraBaseResponse>() {
                @Override
                public void onResponse(Call<HippraBaseResponse> call, Response<HippraBaseResponse> response) {
                    if (response.isSuccessful()) {
                        //Log.d("----login fragment", "request success");
                        if (response.body().getStatus() == HippraApi.REQUEST_SUCCESS) {
                            mSignInLayout.setVisibility(View.VISIBLE);
                            mForgotPasswordLayout.setVisibility(View.INVISIBLE);
                            Toast.makeText(LoginFragment.this.getContext(), R.string.reset_request_success_str, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(LoginFragment.this.getContext(), R.string.reset_request_failed_str, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<HippraBaseResponse> call, Throwable t) {
                    Toast.makeText(LoginFragment.this.getContext(), R.string.reset_request_failed_str, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            mSignInLayout.setVisibility(View.INVISIBLE);
            mForgotPasswordLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int id);
    }
}
